import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home.component';
import { UserEditComponent } from './components/user-edit.component';
import { ArtistListComponent } from './components/artist-list.component';
import { ArtistAddComponent } from './components/artist-add.component';
import { ArtistEditComponent } from './components/artist-edit.component';
import { AlbumListComponent } from './components/album-list.component';
import { SearchComponent } from './components/search.component';

const appRoutes: Routes = [
	{path: '', component: HomeComponent},
	{path: 'artistas', component: ArtistListComponent},
	{path: 'editar-artista/:id', component: ArtistEditComponent},
	{path: 'albums/:id', component: AlbumListComponent},
	{path: 'buscar-artista/:texto', component: SearchComponent},
	{path: 'buscar-album/:texto', component: SearchComponent},
	{path: 'buscar-cancion/:texto', component: SearchComponent},
	{path: 'crear-artista', component: ArtistAddComponent},
	{path: 'mis-datos', component: UserEditComponent},
	{path: '**', component: HomeComponent},
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
