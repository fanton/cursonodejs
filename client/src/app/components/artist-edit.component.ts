import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GLOBAL } from '../services/global';
import { UserService } from '../services/user.service';
import { ArtistService } from '../services/artist.service';
import { Artist } from '../models/artist';

@Component({
	selector: 'artist-edit',
	templateUrl: '../views/artist-edit.html',
	providers: [UserService, ArtistService]
})
export class ArtistEditComponent implements OnInit{
	public titulo: string;
	public artist: Artist;
	public identity;
	public token;
	public url: string;
	public alertMessage;
	
	constructor(
	    private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService,
		private _artistService: ArtistService
	) {
		this.titulo = 'Editar Artista';
		
		// localStorage
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();
		
		this.url = GLOBAL.url;
	}
	
	ngOnInit(){

	}

	getArtist(){
		this._route.params.forEach((params: Params) =>{
			let id = params['id'];
			
			this._artistService.getArtist(this.token, id).subscribe(
				response => {
					if(!response.artist){
						this._router.navigate(['/']);
					}
					else{
						this.artist = response.artist;
					}
				},
				error => {
					var errorMessage = <any>error;
					
					if(errorMessage != null){
						var body = JSON.parse(error._body);
						//this.alertMessage = body.message;
						
						console.log(error);
					}
				}
			);
		});
    }
    
	onSubmit(){
		this._route.params.forEach((params: Params) =>{
			let id = params['id'];
			
			this._artistService.editArtist(this.token, id, this.artist).subscribe(
				response => {
					if(!response.artist){
						this.alertMessage = 'El artista no se ha actualizado';
					}
					else{
						this.alertMessage = 'Datos de artista actualizados';
					}
				},
				error => {
					var errorMessage = <any>error;
					
					if(errorMessage != null){
						var body = JSON.parse(error._body);
						this.alertMessage = body.message;
						console.log(error);
					}
				}
			);
		});
    }
}
