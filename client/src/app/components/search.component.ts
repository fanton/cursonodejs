import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GLOBAL } from '../services/global';
import { UserService } from '../services/user.service';
import { SearchService } from '../services/search.service';

@Component({
	selector: 'search',
	templateUrl: '../views/search.html',
	providers: [UserService, SearchService]
})
export class SearchComponent implements OnInit{
	public titulo: string;
	public texto: string;
	public resultados: string[];
	public identity;
	public token;
	public url: string;
	public alertMessage;
	
	constructor(
	    private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService,
		private _searchService: SearchService
	) {
		this.titulo = 'Buscar';
		
		// localStorage
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();
		
		this.url = GLOBAL.url;
	}
	
	ngOnInit(){

	}

	onSubmit(){
	    this._route.params.forEach((params: Params) => {
            this._searchService.search(this.texto).subscribe(
                response => {
                    if(!response.resultados){
                        this._router.navigate(['/']);
                    }
                    else {
                        this.resultados = response.resultados;
                    }
                },
                error => {
                    var errorMessage = <any>error;

                    if(errorMessage != null){
                        var body = JSON.parse(error._body);
                        this.alertMessage = body.message;
                        console.log(error);
                    }
                }
            )
        });
    }
}
