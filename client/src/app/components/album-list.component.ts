import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GLOBAL } from '../services/global';
import { UserService } from '../services/user.service';
import { AlbumService } from '../services/album.service';
import { Album } from '../models/album';

@Component({
	selector: 'album-list',
	templateUrl: '../views/album-list.html',
	providers: [UserService, AlbumService]
})
export class AlbumListComponent implements OnInit{
	public titulo: string;
	public artistId: string;
	public albums: Album[];
	public identity;
	public token;
	public url: string;
	public alertMessage;
	
	constructor(
	    private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService,
		private _albumService: AlbumService
	) {
		this.titulo = 'Albums';
		
		// localStorage
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();
		
		this.url = GLOBAL.url;
	}
	
	ngOnInit(){
		//console.log('album-list.component.ts cargado);

        // Obtener listado de albums
        //getAlbums();
	}

	getAlbums(){
	    this._route.params.forEach((params: Params) => {
            this._albumService.getAlbumsArtist(this.token, this.artistId).subscribe(
                response => {
                    if(!response.albums){
                        this._router.navigate(['/']);
                    }
                    else {
                        this.albums = response.albums;
                    }
                },
                error => {
                    var errorMessage = <any>error;

                    if(errorMessage != null){
                        var body = JSON.parse(error._body);
                        this.alertMessage = body.message;
                        console.log(error);
                    }
                }
            )
        });
    }
}
