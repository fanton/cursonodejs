import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { GLOBAL } from './global';

@Injectable()
export class SearchService{
	public url: string;
	public identity: string;
	public token: string;
	
	constructor(private _http: Http){
		this.url = GLOBAL.url;
	}
	
	search(texto:string){			
		let headers = new Headers({'Content-Type':'application/json'});

        let options = new RequestOptions({ headers: headers});

        return this._http.get(this.url+'buscar-artista/'+texto, options)
            .pipe(map(res => res.json()));
	}
	
	getIdentity(){
		let identity = JSON.parse(localStorage.getItem('identity'));
		
		if(identity != "undefined") {
			this.identity = identity;
		}
		else {
			this.identity = null;
		}
		
		return this.identity;
	}
	
	getToken() {
		let token = localStorage.getItem('token');

		if(token != "undefined") {
			this.token = token;
		}
		else {
			this.token = null;
		}
		
		return this.token;
	}
}
