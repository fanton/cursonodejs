import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { GLOBAL } from './global';
import { Album } from "../models/album";

@Injectable()
export class AlbumService{
	public url: string;
	
	constructor(private _http: Http){
		this.url = GLOBAL.url;
	}

	// Obtiene los albums de un artista
    getAlbumsArtist(token, id: string){
        let headers = new Headers({
            'Content-Type':'application/json',
            'Authorization': token
        });

        let options = new RequestOptions({ headers: headers});

        return this._http.get(this.url+'albums/'+id, options)
            .pipe(map(res => res.json()));
    }
}
