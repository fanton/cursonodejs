import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { UserService } from './services/user.service';
import { User } from './models/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [UserService]
})
export class AppComponent implements OnInit {
  public title = 'MUSIFY';
  public user: User;
  public user_register: User;
  public identity;
  public token;
  public errorMessage;
  public alertRegister;
  
  constructor(
      private _route: ActivatedRoute,
      private _router: Router,
	private _userService:UserService
  ){
	this.user = new User('', '', '', '', 'ROLE_USER', '');
	this.user_register = new User('', '', '', '', 'ROLE_USER', '');
  }
  
  ngOnInit() {
	this.identity = this._userService.getIdentity();
	this.token = this._userService.getToken();
  }
  
  public onSubmit() {
	//console.log(this.user);
	
	// Conseguir los datos del usuario identificado
	this._userService.signup(this.user).subscribe(
		response => {
			let identity = response.user;
			this.identity = identity;
			
			if(!this.identity._id){
				alert("usuario no identificado");
			}
			else {
				// Crear elemento en localstorge par guardar la sesión del user
				localStorage.setItem('identity', JSON.stringify(identity));
				
				// Conseguir el token pra enviar a cada petición
				this._userService.signup(this.user, true).subscribe(
					response => {
						let token = response.token;
						this.token = token;
						
						if(this.token <= 0){
							alert("Token no generado");
						}
						else {
							// Crear elemento en localstorge par guardar la sesión del user
							localStorage.setItem('token', token);

							this.user = new User('', '', '', '', 'ROLE_USER', '');
						}
					},
					error => {
						var errorMessage = <any>error;
						
						if(errorMessage != null){
							var body = JSON.parse(error._body);
							this.errorMessage = body.message;
							console.log(error);
						}
					}
				);
			}
		},
		error => {
			var errorMessage = <any>error;
			
			if(errorMessage != null){
				var body = JSON.parse(error._body);
				this.errorMessage = body.message;
				console.log(error);
			}
		}
	);
  }
  
  logout() {
	localStorage.removeItem('identity');
	localStorage.removeItem('token');
	localStorage.clear();
	this.identity = null;
	this.token = null;

	this._router.navigate(['/']);
  }
  
  public onSubmitRegister() {
	console.log(this.user_register);
	
	this._userService.register(this.user_register).subscribe(
		response => {
			let user = response.user;
			this.user_register = user;
			
			if(!user._id){
				this.alertRegister = 'Error al registrarse';
			}
			else {
				this.alertRegister = "Registro correcto";
				this.user_register = new User('', '', '', '', 'ROLE_USER', '');
			}
		},
		error => {
			var alertRegister = <any>error;
			
			if(alertRegister != null){
				var body = JSON.parse(error._body);
				this.alertRegister = body.message;
				
				console.log(error);
			}
		}
	);
  }
  
}
