'use strict'

var fs = require('fs');
var path = require('path');

var Artist = require('../models/artist');
var Album = require('../models/album');
var Song = require('../models/song');

function getAlbum(req, res){
	var albumId = req.params.id;
	
	Album.findById(albumId).populate({path: 'artist'}).exec((err, album) => {
		if(err){
			res.status(500).send({message: 'Error el petición Album'});
		}
		else {
			if(!album){
				res.status(404).send({message: 'No existe Album'});
			}
			else{
				res.status(200).send({album});
			}
		}
	});
}

function saveAlbum(req, res){
	var album = new Album();
	
	var params = req.body;
	album.title = params.title;
	album.description = params.description;
	album.year = params.year;
	album.image = 'null';
	album.artist = params.artist;
	
	album.save((err, albumStored) => {
		if(err){
			res.status(500).send({message: 'Error al guardar Album'});
		}
		else {
			if(!albumStored){
				res.status(404).send({message: 'No se ha guardado Album'});
			}
			else{
				res.status(200).send({album: albumStored});
			}
		}
	});
}

function updateAlbum(req, res){
	var albumId = req.params.id;
	var update = req.body;
	
	Album.findByIdAndUpdate(albumId, update, (err, albumUpdated) => {
		if(err){
			res.status(500).send({message: 'Error al guardar Album'});
		}
		else {
			if(!albumUpdated){
				res.status(404).send({message: 'No se actualizó Album'});
			}
			else{
				res.status(200).send({album: albumUpdated});
			}
		}
	});
}

function deleteAlbum(req, res){
	var albumId = req.params.id;
	
	Album.findByIdAndRemove(albumId, (err, albumRemoved)=>{
		if(err){
			res.status(500).send({message: 'Error al eliminar Album'});
		}
		else {
			if(!albumRemoved){
				res.status(404).send({message: 'No se eliminó Album'});
			}
			else{
				// Borrar canciones de album
				Song.find({album: albumRemoved._id}).remove((err, songRemoved)=>{
					if(err){
						res.status(500).send({message: 'Error al eliminar Canción'});
					}
					else {
						if(!songRemoved){
							res.status(404).send({message: 'No se eliminó Canción'});
						}
						else{		
							res.status(200).send({albumRemoved});
						}
					}
				});
			}
		}
	});
}

function uploadImage(req, res){
	var albumId = req.params.id;
	var file_name = 'No subido';
	
	if(req.files){
		var file_path = req.files.image.path;
		var file_split = file_path.split('\\');
		var file_name = file_split[2];
		
		Album.findByIdAndUpdate(albumId, {image: file_name}, (err, albumUpdated) => {
			if(err){
				res.status(500).send({message: 'Error al actualizar imagen de album'});
			}
			else{
				if(!albumUpdated){
					res.status(404).send({message: 'La imagen del album no se pudo actualizar'});
				}
				else{
					res.status(200).send({album: albumUpdated});
				}
			}
		});
	}
	else {
		res.status(200).send({message: 'No se ha subido imagen'});
	}
}

function getImageFile(req, res){
	var imageFile = req.params.imageFile;
	var path_file = './uploads/albums/'+imageFile;
	
	fs.exists(path_file, function(exists){
		if(exists){
			res.sendFile(path.resolve(path_file));
		}
		else {
			res.status(200).send({message: 'No existe la imagen de album'});
		}
	});
}

// Si recibe artistId Devuelve los albums de un artist, sino todos
function getAlbums(req, res){
	var artistId = req.params.id;
	
	if(!artistId){
		var find = Album.find({}).sort('title');
	}
	else{
		var find = Album.find({artist: artistId}).sort('year');
	}
	
	find.populate({path: 'artist'}).exec((err, albums) => {
		if(err){
			res.status(500).send({message: 'Error el petición Albums'});
		}
		else {
			if(!albums){
				res.status(404).send({message: 'No existen Albums'});
			}
			else{
				res.status(200).send({albums});
			}
		}
	});
}

module.exports = {
	getAlbum,
	saveAlbum,
	updateAlbum,
	deleteAlbum,
	uploadImage,
	getImageFile,
	getAlbums
};
