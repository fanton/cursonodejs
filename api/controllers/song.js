'use strict'

var fs = require('fs');
var path = require('path');

var Artist = require('../models/artist');
var Album = require('../models/album');
var Song = require('../models/song');

function getSong(req, res){
	var songId = req.params.id;
	
	Song.findById(songId).populate({path: 'album'}).exec((err, song) => {
		if(err){
			res.status(500).send({message: 'Error el petición Song'});
		}
		else {
			if(!song){
				res.status(404).send({message: 'No existe Song'});
			}
			else{
				res.status(200).send({song});
			}
		}
	});
}

function saveSong(req, res){
	var song = new Song();
	
	var params = req.body;
	song.number = params.number;
	song.name = params.name;
	song.duration = params.duration;
	album.file = 'null';
	song.album = params.album;
	
	song.save((err, songStored) => {
		if(err){
			res.status(500).send({message: 'Error al guardar Song'});
		}
		else {
			if(!songStored){
				res.status(404).send({message: 'No se ha guardado Song'});
			}
			else{
				res.status(200).send({song: songStored});
			}
		}
	});
}

function updateSong(req, res){
	var songId = req.params.id;
	var update = req.body;
	
	Song.findByIdAndUpdate(songId, update, (err, songUpdated) => {
		if(err){
			res.status(500).send({message: 'Error al guardar Song'});
		}
		else {
			if(!songUpdated){
				res.status(404).send({message: 'No se actualizó Song'});
			}
			else{
				res.status(200).send({songUpdated});
			}
		}
	});
}

function deleteSong(req, res){
	var songId = req.params.id;
	
	Song.findByIdAndRemove(songId, (err, songRemoved)=>{
		if(err){
			res.status(500).send({message: 'Error al eliminar Song'});
		}
		else {
			if(!songRemoved){
				res.status(404).send({message: 'No se eliminó Song'});
			}
			else{
				res.status(200).send({song: songRemoved});
			}
		}
	});
}

function uploadFile(req, res){
	var songId = req.params.id;
	var file_name = 'No subido';
	
	if(req.files){
		var file_path = req.files.image.path;
		var file_split = file_path.split('\\');
		var file_name = file_split[2];
		
		Song.findByIdAndUpdate(songId, {file: file_name}, (err, songUpdated) => {
			if(err){
				res.status(500).send({message: 'Error al actualizar file de song'});
			}
			else{
				if(!songUpdated){
					res.status(404).send({message: 'File de song no se pudo actualizar'});
				}
				else{
					res.status(200).send({song: songUpdated});
				}
			}
		});
	}
	else {
		res.status(200).send({message: 'No se ha subido file'});
	}
}

function getSongFile(req, res){
	var songFile = req.params.songFile;
	var path_file = './uploads/songs/'+songFile;
	
	fs.exists(path_file, function(exists){
		if(exists){
			res.sendFile(path.resolve(path_file));
		}
		else {
			res.status(200).send({message: 'No existe file de song'});
		}
	});
}

// Si recibe artistId Devuelve los albums de un artist, sino todos
function getSongs(req, res){
	var albumId = req.params.id;
	
	if(!albumId){
		var find = Song.find({}).sort('number');
	}
	else{
		var find = Song.find({album: albumId}).sort('number');
	}
	
	find.populate({
		path: 'album',
		populate: {
			path: 'artist',
			model: 'Artist'
		}
	}).exec((err, songs) => {
		if(err){
			res.status(500).send({message: 'Error el petición Songs'});
		}
		else {
			if(!albums){
				res.status(404).send({message: 'No existen Songs'});
			}
			else{
				res.status(200).send({songs});
			}
		}
	});
}

module.exports = {
	getSong,
	saveSong,
	updateSong,
	deleteSong,
	uploadFile,
	getSongFile,
	getSongs
};
