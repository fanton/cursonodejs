'use strict'

var fs = require('fs');
var path = require('path');
var bcrypt = require('bcrypt-nodejs');
var User = require('../models/user');
var jwt = require('../services/jwt');

function pruebas(req, res) {
	res.status(200).send({
		message: 'Pruebaaa'
		});
}

function saveUser(req, res) {
	var user = new User();
	
	var params = req.body;
	
	user.name = params.name;
	user.surname = params.surname;
	user.email = params.email;
	user.role = 'ROLE_USER';
	user.image = 'null';
	
	if(params.password) {
		// Encriptar
		bcrypt.hash(params.password, null, null, function(err, hash){
			user.password = hash;
			
			if(user.name != null && user.surname != null && user.email != null){
				// Guardo el user
				user.save((err, userStored) => {
					if(err){
						res.status(500).send({message: 'Error al guardar user'});
					}
					else{
						if(!userStored){
							res.status(404).send({message: 'No se ha registrado user'});
						}
						else {
							res.status(200).send({user: userStored});
						}
					}
				});
			}
			else{
				res.status(200).send({message: 'faltan datos de user'});
			}
		});
	}
	else {
		res.status(200).send({message: 'sin pass'});
	}
}

function loginUser(req, res) {
	var params = req.body;
	
	var email = params.email;
	var password = params.password;
	
	User.findOne({email: email.toLowerCase()}, (err, user) => {
		if(err){
			res.status(500).send({message: 'Error en petición de login'});
		}
		else {
			if(!user){
				res.status(404).send({message: 'User no existe'});
			}
			else {
				// Comprobar la pass
				bcrypt.compare(password, user.password, function(err, check) {
					if(check){
						// devolver los datos logueado
						if(params.gethash){
							// devolver token de jwt
							res.status(200).send({
								token: jwt.createToken(user)
							});
						}
						else {
							res.status(200).send({user});
						}
					}
					else {
						res.status(404).send({message: 'El usuario no se pudo loguear'});
					}
				});
			}
		}
	});
}

function updateUser(req, res){
	var userId = req.params.id;
	var update = req.body;
	
	if(userId != req.user.sub){
		return res.status(500).send({message: 'No tiene permiso para actulizar user'});
	}
	
	User.findByIdAndUpdate(userId, update, (err, userUpdated) => {
		if(err){
			res.status(500).send({message: 'Error al actualizar user'});
		}
		else{
			if(!userUpdated){
				res.status(404).send({message: 'El usuario no se pudo actualizar'});
			}
			else{
				res.status(200).send({user: userUpdated});
			}
		}
	});
}

function uploadImage(req, res){
	var userId = req.params.id;
	var file_name = 'No subido';
	
	if(req.files){
		var file_path = req.files.image.path;
		var file_split = file_path.split('\\');
		var file_name = file_split[2];
		
		User.findByIdAndUpdate(userId, {image: file_name}, (err, userUpdated) => {
			if(err){
				res.status(500).send({message: 'Error al actualizar imagen de user'});
			}
			else{
				if(!userUpdated){
					res.status(404).send({message: 'La imagen del usuario no se pudo actualizar'});
				}
				else{
					res.status(200).send({image: file_name, user: userUpdated});
				}
			}
		});
	}
	else {
		res.status(200).send({message: 'No se ha subido imagen'});
	}
}

function getImageFile(req, res){
	var imageFile = req.params.imageFile;
	var path_file = './uploads/users/'+imageFile;
	
	fs.exists(path_file, function(exists){
		if(exists){
			res.sendFile(path.resolve(path_file));
		}
		else {
			res.status(200).send({message: 'No existe la imagen'});
		}
	});
}

module.exports = {
	pruebas,
	saveUser,
	loginUser,
	updateUser,
	uploadImage,
	getImageFile
};
