'use strict'

var fs = require('fs');
var path = require('path');

var Artist = require('../models/artist');
var Album = require('../models/album');
var Song = require('../models/song');

function getArtist(req, res){
	var artistId = req.params.id;
	
	Artist.findById(artistId, (err, artist) => {
		if(err){
			res.status(500).send({message: 'Error el petición Artist'});
		}
		else {
			if(!artist){
				res.status(404).send({message: 'No existe Artist'});
			}
			else{
				res.status(200).send({artist});
			}
		}
	});
}

function saveArtist(req, res){
	var artist = new Artist();
	
	var params = req.body;
	artist.name = params.name;
	artist.description = params.description;
	artist.image = 'null';
	
	artist.save((err, artistStored) => {
		if(err){
			res.status(500).send({message: 'Error al guardar Artist'});
		}
		else {
			if(!artistStored){
				res.status(404).send({message: 'No se ha guardado Artist'});
			}
			else{
				res.status(200).send({artist: artistStored});
			}
		}
	});
}

function updateArtist(req, res){
	var artistId = req.params.id;
	var update = req.body;
	
	Artist.findByIdAndUpdate(artistId, update, (err, artistUpdated) => {
		if(err){
			res.status(500).send({message: 'Error al guardar Artist'});
		}
		else {
			if(!artistUpdated){
				res.status(404).send({message: 'No se actualizó Artist'});
			}
			else{
				res.status(200).send({artist: artistUpdated});
			}
		}
	});
}

function deleteArtist(req, res){
	var artistId = req.params.id;
	
	Artist.findByIdAndRemove(artistId, (err, artistRemoved) => {
		if(err){
			res.status(500).send({message: 'Error al eliminar Artist'});
		}
		else {
			if(!artistRemoved){
				res.status(404).send({message: 'No se eliminado Artist'});
			}
			else{
				// Borrar Albums de ese artist
				Album.find({artist: artistRemoved._id}).remove((err, albumRemoved)=>{
					if(err){
						res.status(500).send({message: 'Error al eliminar Album'});
					}
					else {
						if(!albumRemoved){
							res.status(404).send({message: 'No se eliminó Album'});
						}
						else{
							// Borrar canciones de album
							Song.find({album: albumRemoved._id}).remove((err, songRemoved)=>{
								if(err){
									res.status(500).send({message: 'Error al eliminar Canción'});
								}
								else {
									if(!songRemoved){
										res.status(404).send({message: 'No se eliminó Canción'});
									}
									else{		
										res.status(200).send({artist: artistRemoved});
									}
								}
							});
						}
					}
				});
			}
		}
	});
}

function uploadImage(req, res){
	var artistId = req.params.id;
	var file_name = 'No subido';
	
	if(req.files){
		var file_path = req.files.image.path;
		var file_split = file_path.split('\\');
		var file_name = file_split[2];
		
		Artist.findByIdAndUpdate(artistId, {image: file_name}, (err, artistUpdated) => {
			if(err){
				res.status(500).send({message: 'Error al actualizar imagen de artist'});
			}
			else{
				if(!artistUpdated){
					res.status(404).send({message: 'La imagen del artist no se pudo actualizar'});
				}
				else{
					res.status(200).send({artist: artistUpdated});
				}
			}
		});
	}
	else {
		res.status(200).send({message: 'No se ha subido imagen'});
	}
}

function getImageFile(req, res){
	var imageFile = req.params.imageFile;
	var path_file = './uploads/artists/'+imageFile;
	
	fs.exists(path_file, function(exists){
		if(exists){
			res.sendFile(path.resolve(path_file));
		}
		else {
			res.status(200).send({message: 'No existe la imagen de artist'});
		}
	});
}

module.exports = {
	getArtist,
	saveArtist,
	updateArtist,
	deleteArtist,
	uploadImage,
	getImageFile
};
