'use strict'

var fs = require('fs');
var path = require('path');

var Artist = require('../models/artist');
var Album = require('../models/album');
var Song = require('../models/song');

function searchArtist(req, res){
	var texto = req.params.texto;
	
	Artist.find({ name: texto }, 'name', (err, resultado) => {
		if(err){
			res.status(500).send({message: 'Error en petición Search artist'});
		}
		else {
			if(!resultado){
				res.status(404).send({message: 'No existe Busqueda de artist'});
			}
			else{
				res.status(200).send({resultado});
			}
		}
	});
}

function searchAlbum(req, res){
	var texto = req.params.texto;
	
	Album.find({ title: texto }, 'title', (err, resultado) => {
		if(err){
			res.status(500).send({message: 'Error en petición Search album'});
		}
		else {
			if(!resultado){
				res.status(404).send({message: 'No existe Busqueda de album'});
			}
			else{
				res.status(200).send({resultado});
			}
		}
	});
}

function searchSong(req, res){
	var texto = req.params.texto;
	
	Song.find({ name: texto }, 'name', (err, resultado) => {
		if(err){
			res.status(500).send({message: 'Error en petición Search song'});
		}
		else {
			if(!resultado){
				res.status(404).send({message: 'No existe Busqueda de song'});
			}
			else{
				res.status(200).send({resultado});
			}
		}
	});
}

module.exports = {
	searchArtist,
	searchAlbum,
	searchSong
};
