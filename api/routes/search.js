'use strict'

var express = require('express');
var SearchController = require('../controllers/search');

var api = express.Router();
var md_auth = require('../middlewares/aunthenticated');

api.get('/buscar-artista/:texto', SearchController.searchArtist);
api.get('/buscar-album/:texto', SearchController.searchAlbum);
api.get('/buscar-cancion/:texto', SearchController.searchSong);

module.exports = api;
